#!/usr/bin/env python
# -*- coding: utf-8 -*-

VERSION = "0.2.1"
VERSION_MAJOR_MINOR = ".".join(VERSION.split(".")[0:2])
APPNAME = "Gen-ly"

PKGNAME = "gen-ly"
PKGVERSION = "0.6.0"

srcdir = "."
blddir = "build"

# -----------------------------------------------------------------------------
def set_options(opt):
    opt.tool_options('compiler_cc')
    opt.tool_options('gnu_dirs')

# -----------------------------------------------------------------------------
def configure(conf):
    conf.check_tool('compiler_cc vala gnu_dirs')

    conf.check_cfg(package='glib-2.0', uselib_store='GLIB',
                   atleast_version='2.14.0', mandatory=True,
                   args='--cflags --libs')
    conf.check_cfg(package='gobject-2.0', uselib_store='GOBJECT',
                   atleast_version='2.14.0', mandatory=True,
                   args='--cflags --libs')
    conf.check_cfg(package='gee-1.0', uselib_store='GEE',
                   atleast_version='0.5.0', mandatory=True,
                   args='--cflags --libs')
    conf.check_cfg(package='json-glib-1.0', uselib_store='JSON-GLIB',
                   atleast_version='0.10.0', mandatory=True,
                   args='--cflags --libs')
    conf.check_cfg(package='gio-2.0', uselib_store='GIO',
                   atleast_version='2.26.0', mandatory=True,
                   args='--cflags --libs')
    conf.check_cfg(package='gtk+-2.0', uselib_store='GTK+',
                   atleast_version='2.10.0', mandatory=True,
                   args='--cflags --libs')
    
    conf.define('PACKAGE', PKGNAME)
    conf.define('PACKAGE_NAME', PKGNAME)
    conf.define('PACKAGE_STRING', PKGNAME + '-' + PKGVERSION)
    conf.define('PACKAGE_VERSION', PKGNAME + '-' + PKGVERSION)
    conf.define('VERSION', VERSION)
    conf.define('VERSION_MAJOR_MINOR', VERSION_MAJOR_MINOR)
    
    
def build(bld):
    bld.add_subdirs('src/api')
    bld.add_subdirs('src/cli')



