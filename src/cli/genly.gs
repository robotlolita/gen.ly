/* Genly.gs
 * 
 * -----------------------------------------------------------------------------
 * Copyright (c) 2010 Quildreen Motta
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * -----------------------------------------------------------------------------
 * 
 * Command line interface for shortening URLs with Bit.ly
 * 
 * Author:
 *      Quildreen Motta <quildreen@gmail.com>
 */

//--[ CONFIGURATION PARMS ]-----------------------------------------------------
api:       Bitly
api_login: string
api_key:   string
cb_text:   StringBuilder
copy:      bool
verbose:   bool

//--[ ACTIONS ]-----------------------------------------------------------------
show_expand: bool
show_clicks: bool
show_referrers: bool
show_titles: bool
show_countries: bool


//--[ ARGUMENT PARSING ]--------------------------------------------------------
const cli_args: array of OptionEntry = {
	{"login", 'u', 0, OptionArg.STRING, out api_login, "Bit.ly API login."
	,null},
	
	{"apikey", 'k', 0, OptionArg.STRING, out api_key, "Bit.ly API key.", null},
	
	{"expand", 0, 0, OptionArg.NONE, out show_expand
	,"Expands the given URLs or hashs", null},
	
	{"clicks", 0, 0, OptionArg.NONE, out show_clicks
	,"Prints a table of clicks for the given URLs or hashs", null},
	
	{"referrers", 0, 0, OptionArg.NONE, out show_referrers
	,"Prints a list of referrers for the given URLs or hashs", null},
	
	{"titles", 0, 0, OptionArg.NONE, out show_titles
	,"Prints the titles of the given URLs or hashs", null},
	
	{"countries", 0, 0, OptionArg.NONE, out show_countries
	,"Prints a list of countries that accessed the given URLs/hashs.", null},
	
	{"copy", 0, 0, OptionArg.NONE, out copy
	,"Copy the resulting URLs to the clipboard", null},
	
	{"verbose", 'v', 0, OptionArg.NONE, out verbose
	,"Prints lots of useless information.", null},
	
	{null}
}


//--[ FUNCTIONS ]---------------------------------------------------------------
/******************************************************************************
 * Returns the minor value between two integers                               *
 ******************************************************************************/
def inline min(a: int, b: int): int
	return (a < b) ? a : b


/******************************************************************************
 * Validates the API info                                                     *
 ******************************************************************************/
def validate(): bool raises BitlyError, Error
	var result = api.validate(api_login, api_key)
	if not result
		print "Couldn't connect with the API. Invalid login or API key."
	
	return result


/******************************************************************************
 * Expands the given set of URLs                                              *
 ******************************************************************************/
def expand(args: array of string) raises BitlyError, Error
	var i = 0
	while i < args.length
		var len  = min(i + 15, args.length)
		var urls = args[i:len]
		var data = api.expand(urls)
		var j    = 0
		for var url in data
			if verbose do print "%s => %s", args[i + j], url
			else       do print "%s", url
			if copy do copy_to_clipboard(url + "\n")
			j++
		i += 15


/******************************************************************************
 * Shortens the given set of URLs                                             *
 ******************************************************************************/
def shorten(args: array of string) raises BitlyError, Error
	var i = 0
	while i < args.length
		var len  = min(i + 15, args.length)
		var urls = args[i:len]
		var data = api.shorten(urls)
		var j    = 0
		for var url in data
			if verbose do print "%s => %s", args[i + j], url
			else       do print "%s", url
			if copy do copy_to_clipboard(url + "\n")
			j++
		i += 15


/******************************************************************************
 * Shows the number of clicks for the given URLs                              *
 ******************************************************************************/
def clicks(args: array of string) raises BitlyError, Error
	var titles = new array of string[0]	
	var i = 0
	while i < args.length
		var len  = min(i + 15, args.length)
		var urls = args[i:len]
		var data = api.clicks(urls)
		
		if show_titles do titles = api.titles(urls)
		for var j = 0 to (data.length[0] - 1)
			url: string
			if show_titles do url = titles[i + j]
			else           do url = args[i + j]
			var u   = (int)data[i + j, 0]
			var g   = (int)data[i + j, 1]
			print "%s\nuser clicks: %d\tglobal clicks: %d\n", url, u, g
		i += 15


/******************************************************************************
 * Shows referrer data for the given URLs                                     *
 ******************************************************************************/
def referrers(args: array of string) raises BitlyError, Error
	for var url in args
		var data = api.referrers(url)
		if show_titles
			print "Referrers for %s", api.titles({url})[0]
		else
			print "Referrers for %s", url
		
		for var referrer in data.keys
			print "%s: %d\n", referrer, (int)data[referrer]
		print ""


/******************************************************************************
 * Shows the title for the given URLs                                         *
 ******************************************************************************/
def titles(args: array of string) raises BitlyError, Error
	var i = 0
	while i < args.length
		var len  = min(i + 15, args.length)
		var urls = args[i:len]
		var data = api.titles(urls)
		var j    = 0
		for var title in data
			if verbose do print "%s => %s", args[i + j], title
			else       do print "%s", title
			if copy do copy_to_clipboard(title + "\n")
			j++
		i += 15


/******************************************************************************
 * Show country click data for the given URLs                                 *
 ******************************************************************************/
def countries(args: array of string) raises BitlyError, Error
	for var url in args
		var data = api.countries(url)
		if show_titles
			print "Country data for %s", api.titles({url})[0]
		else
			print "Country data for %s", url
		
		for var country in data.keys
			print "%s:\t%d\n", country, (int)data[country]
		print ""


/******************************************************************************
 * Appends a string to the clipboard                                          *
 ******************************************************************************/
def copy_to_clipboard(text: string)
	cb_text.append(text)
	var d  = Gdk.Display.open_default_libgtk_only()
	var cb = Gtk.Clipboard.get_for_display(d, Gdk.SELECTION_CLIPBOARD)
	cb.set_text(cb_text.str, -1)
	cb.store()
	
	d  = null
	cb = null


/******************************************************************************
 * Prints a CLI parsing error                                                 *
 ******************************************************************************/
def error(message: string)
	print "%s\n", message
	print "Run 'genly --help' to see a full list of available options.\n"


/******************************************************************************
 * Parses the command line arguments                                          *
 ******************************************************************************/
def parse_arguments(ref args:weak array of string)
	try
		var ctx = new OptionContext(" url -- Command line interface for Bit.ly")
		ctx.set_help_enabled(true)
		ctx.add_main_entries(cli_args, null)
		ctx.parse(ref args)
	except e: OptionError
		error(e.message)
		return
	if args.length < 2 do
		error("No URL specified.")
		return


// --[ MAIN ]-------------------------------------------------------------------
init
	/* inits Gtk+ so we can copy stuff to the clipboard */
	Gtk.init(ref args)
	parse_arguments(ref args)
	
	/* if no login/api key pair was given, use the default ones */
	if api_login is null or api_key is null
		api_login = "geniely"
		api_key   = "R_4a87acf5573804997b26cdcc36603c77"
	
	/* initialize more stuff */
	cb_text = new StringBuilder
	args    = args[1:args.length]
	api     = new Bitly(api_login, api_key)
	
	/* execute stuff */	
	try
		if not validate() do return
		
		if      show_expand    do expand(args)
		else if show_clicks    do clicks(args)
		else if show_referrers do referrers(args)
		else if show_countries do countries(args)
		else if show_titles    do titles(args)
		else                   do shorten(args)
	except e:BitlyError
		print "Error requesting data from the API: %s", e.message
	except e:Error
		print "Couldn't connect with the API: %s", e.message
