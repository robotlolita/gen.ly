/* Gen.ly.gs
 * 
 * -----------------------------------------------------------------------------
 * Copyright (c) 2010 Quildreen Motta
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * -----------------------------------------------------------------------------
 * 
 * Wrapper on ``bit.ly`` API for shortening URLs and tracking.
 * 
 * Author:
 *      Quildreen Motta <quildreen@gmail.com>
 */
 
 
/******************************************************************************
 * Exception domain for Bit.ly request errors                                 *
 ******************************************************************************/
exception BitlyError
	/**************************************************************************
	 * API request limit exceded (HTTP 403)                                   *
	 *                                                                        *
	 * Bit.ly limits the number of concurrent requests by IP and also the     *
	 * number of requests per hour, minutes and seconds. Limits reset every   *
	 * hour.                                                                  *
	 *                                                                        *
	 * For more info on Bit.ly's API request limits see:                      *
	 * http://code.google.com/p/bitly-api/wiki/ApiDocumentation#Rate_Limiting *
	 **************************************************************************/
	RATE_LIMIT
	
	/**************************************************************************
	 * A bad request was made to an API endpoint (HTTP 500)                   *
	 *                                                                        *
	 * Possibly the request query is missing parameters, or have too much     *
	 * parameters, or the URI requested is invalid.                           *
	 **************************************************************************/
	BAD_REQUEST
	
	/**************************************************************************
	 * API gave an unknow error or is temporary unavailable (HTTP 503)        *
	 **************************************************************************/
	UNKNOW
	
	/**************************************************************************
	 * Requested URL was not found                                            *
	 **************************************************************************/
	URL_NOT_FOUND


/******************************************************************************
 * Holds info about a given bit.ly link                                       *
 ******************************************************************************/
class BitlyInfo: Object
	created_by:  string  /* user that created this link */
	global_hash: string  /* global hash for this link */
	user_hash:   string  /* user hash for this link */
	title:       string  /* title of this link */
	
	
	construct (by: string, ghash: string, uhash: string, title: string?)
		self.created_by  = by
		self.global_hash = ghash
		self.user_hash   = uhash
		self.title       = title


/******************************************************************************
 * Wrapper for Bit.ly REST API                                                *
 ******************************************************************************/
class Bitly: Object
	_login  : string
	_apikey : string
	_apiurl : string
	
	
	/**************************************************************************
	 * Creates a new instance of the API handler with the given auth info.    *
	 *                                                                        *
	 * @param login: the user's login name                                     *
	 * @param key:   the user's API key                                        *
	 * @param url:   the URL used to make the API requests. Defaults to        *
	 *               "http://api.bit.ly/v3/"                                   *
	 **************************************************************************/
	construct (login: string, key: string, url: string? = null)
		_login  = login
		_apikey = key
		_apiurl = url is not null ? url : "http://api.bit.ly/v3/"
	
	
	/**************************************************************************
	 * Returns a query string for the given array of parameters               *
	 *                                                                        *
	 * Builds a query string from the given array of parameters, so it can be *
	 * appended to the request URL.                                           *
	 *                                                                        *
	 * Each array item must be in the format "key=value".                     *
	 *                                                                        *
	 * The returned string is a general URL query, starting with "?". All     *
	 * values are properly escaped.                                           *
	 *                                                                        *
	 * @param parms: array of the query parameters                            *
	 * @return the query string                                               *
	 **************************************************************************/
	def get_query(parms: array of string): string
		var rv  = new StringBuilder
		var sep = ""
		
		for var p in parms
			var str   = p.split("=", 2)
			var key   = str[0]
			var value = Uri.escape_string(str[1], "$-_.+!*'(),", true)
			
			rv.append(sep + key + "=" + value)
			sep = "&"
		
		rv.append(sep + "format=json&login=" + _login + "&apiKey=" + _apikey)
		return rv.str
	
	
	/**************************************************************************
	 * Returns a list of correct short URL/hash parameters for a given list   *
	 * of short URL/hashs.                                                    *
	 *                                                                        *
	 * The given URL array must have at most 15 elements, as that's the hard  *
	 * limit imposed by Bit.ly's API                                          *
	 *                                                                        *
	 * @param urls: array of short URLs / hashes                              *
	 * @return the query string for the given URL array.                      *
	 **************************************************************************/
	def get_url_parms(urls: array of string): array of string
		assert urls.length <= 15
		
		var query = new array of string[0]
		for var url in urls
			if url.has_prefix("http://")
				query += "shortUrl=" + url
			else
				query += "hash=" + url
				
		return query
	
	
	/**************************************************************************
	 * Returns a JSON object for the given request                            *
	 *                                                                        *
	 * Makes a Request to the given Bit.ly API endpoint and returns the JSON  *
	 * response.                                                              *
	 *                                                                        *
	 * Raises an Error in case it fails to connect to the API endpoint, and *
	 * a BitlyError when the server returns a bad response.                   *
	 *                                                                        *
	 * @param endpoint: API request endpoint ("shorten", "expand", ...)       *
	 * @param parms:    array of request parameters. Login and URL are        *
	 *                  automatically included.                               *
	 * @return the ``data`` JSON object, in a successful request.             *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return the JSON object constructed from the API's response.           *
	 **************************************************************************/
	def request(endpoint: string, parms: array of string): Json.Object \
	raises Error, BitlyError
		/* loads the API url and parse the JSON received */
		result: string
		
		// TODO: cache the requests to prevent over-stressing the API
		var f = File.new_for_uri(_apiurl + endpoint + "?" + get_query(parms))
		f.load_contents(null, out result)
		
		var parser = new Json.Parser
		parser.load_from_data(result)
		
		/* checks if the stuff is okay, then return the data object */
		var root   = parser.get_root().get_object()
		var scode  = root.get_int_member("status_code")
		var status = root.get_string_member("status_txt")
		
		if scode == 200
			return root.get_object_member("data")
		else
			case scode
				when 403 do raise new BitlyError.RATE_LIMIT(status)
				when 500 do raise new BitlyError.BAD_REQUEST(status)
				default  do raise new BitlyError.UNKNOW(status)
	
	
	/**************************************************************************
	 * Returns an array of short links for the given URLs.                    *
	 *                                                                        *
	 * Bit.ly accepts up to 15 URLs in the same request.                      *
	 *                                                                        *                                                                        *
	 * @param urls:   the array of URLs to shorten.                           *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a string with the shortened url.                               *
	 **************************************************************************/
	def shorten(urls: array of string): array of string \
	raises Error, BitlyError
		var query = new array of string[urls.length]
		for var i = 0 to (urls.length - 1)
			query[i] = "url=" + urls[i]
		
		var req  = request("lookup", query)
		var data = req.get_array_member("lookup")
		
		var rv = new array of string[urls.length]
		for var i = 0 to (urls.length - 1)
			var uinfo = data.get_object_element(i)
			rv[i] = uinfo.get_string_member("short_url")
		
		return rv
	
	
	/**************************************************************************
	 * Shortens a single URL.                                                 *
	 *                                                                        *                                                                        *
	 * @param url:    the long URL to shorten.                                *
	 * @param domain: optional domain for the shortened URL. Defaults to      *
	 *                bit.ly                                                  *
	 * @param login:  the username on which behalf this request will be made. *
	 * @param apikey: api key for the given login.                            *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a string with the shortened url.                               *
	 **************************************************************************/
	def shorten_url(url: string, domain: string = "", login: string = ""
	           ,apikey: string = ""): string \
	raises Error, BitlyError
		var data = request("shorten", {"longUrl="  + url
		                              ,"domain="   + domain
		                              ,"x_login="  + login
		                              ,"x_apiKey=" + apikey})
		return data.get_string_member("url")
	
	
	/**************************************************************************
	 * Expands multiple short urls or hashs.                                  *
	 *                                                                        *
	 * Bit.ly accepts up to 15 URL/hashs in the same request.                 *
	 *                                                                        *
	 * @param urls:  a list of short URLs or hashs to expand                  *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return an array of the long urls for the given hashs/short urls.      *
	 **************************************************************************/
	def expand(urls: array of string): array of string \
	raises Error, BitlyError
		var req  = request("expand", get_url_parms(urls))
		var data = req.get_array_member("expand")
		
		var rv   = new array of string[urls.length]
		for var i = 0 to (data.get_length() - 1)
			var uinfo = data.get_object_element(i)
			if uinfo.has_member("error")
				var error = uinfo.get_string_member("error")
				raise new BitlyError.URL_NOT_FOUND(error)
			rv[i] = uinfo.get_string_member("long_url")
		
		return rv
	
	
	/**************************************************************************
	 * Expands a single URL                                                   *
	 *                                                                        *
	 * @param url: URL or hash to expand                                      *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a string with the expanded url for the given short url/hash.   *
	 **************************************************************************/
	def expand_url(url: string): string \
	raises Error, BitlyError
		return expand({url})[0]
	
	
	/**************************************************************************
	 * Validates a given pair of login/api key.                               *
	 *                                                                        *
	 * For any given pair of login and api key, it returns whether it's valid *
	 * and active.                                                            *
	 *                                                                        *
	 * @param login:  the username to test                                    *
	 * @param apikey: the correspondent API key                               *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a boolean indicating whether the user info is valid or not.    *
	 **************************************************************************/
	def validate(login: string, apikey: string): bool \
	raises Error, BitlyError
		var data = request("validate", {"x_login="  + login
		                               ,"x_apiKey=" + apikey})
		return data.get_int_member("valid") == 1
	
	
	/**************************************************************************
	 * Returns the user/global clicks for the given URL/hashes.               *
	 *                                                                        *
	 * The return value is an array of ``{user_clicks, global_clicks}``,      *
	 * where ``user_clicks`` is the number of clicks on the shortened url     *
	 * from the requesting user, and ``global_clicks`` is the total number    *
	 * of clicks for the given URL.                                           *
	 *                                                                        *
	 * @param urls: list of URL or hashes to retrieve statistics from.        *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return an array of {int, int} (user/global clicks) for the given URLs * 
	 **************************************************************************/ 
	def clicks(urls: array of string): array of int64[,] \
	raises Error, BitlyError
		var req  = request("clicks", get_url_parms(urls))
		var data = req.get_array_member("clicks")
		
		var len = data.get_length()
		var rv  = new array of int64[len,2]
		for var i = 0 to (len - 1)
			var uinfo = data.get_object_element(i)
			if uinfo.has_member("error")
				var error = uinfo.get_string_member("error")
				raise new BitlyError.URL_NOT_FOUND(error)
				
			rv[i, 0] = uinfo.get_int_member("user_clicks")
			rv[i, 1] = uinfo.get_int_member("global_clicks")
			
		return rv
	
	
	/**************************************************************************
	 * Returns a dict of referring sites for a given bit.ly short link and    *
	 * the number of clicks per referrer                                      *
	 *                                                                        *
	 * It accepts either a short url or a hash as parameter, albeit only one  *
	 * url can be requested at a time                                         *
	 *                                                                        *
	 * @param url: short url or hash to lookup.                               *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a dict mapping referrer sites to the number of clicks.         *
	 **************************************************************************/
	def referrers(url: string): dict of string, int64? \
	raises Error, BitlyError
		var req  = request("referrers", get_url_parms({url}))
		var data = req.get_array_member("referrers")
		
		var rv = new dict of string, int64?
		var i  = data.get_length()
		while (i--) > 0
			var uinfo    = data.get_object_element(i)
			var clicks   = uinfo.get_int_member("clicks")
			referrer: weak Json.Node = uinfo.dup_member("referrer")
			if referrer.is_null()
				rv["None"] = clicks
			else
				rv[referrer.get_string()] = clicks
		
		return rv
	
	
	/**************************************************************************
	 * Returns a dict of clicks per country.                                  *
	 *                                                                        *
	 * It accepts either a short url or a hash as parameter, albeit only one  *
	 * url can be requested at a time.                                        *
	 *                                                                        *
	 * The countries are mapped by a two-letter ISO code, but in the event    *
	 * where Bit.ly API returns null for the country code, it'll be referred  *
	 * as ``Unknow".                                                          *
	 *                                                                        *
	 * @param url: short url or hash to lookup.                               *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a dict mapping countries to the number of clicks.              *
	 **************************************************************************/
	def countries(url: string): dict of string, int64? \
	raises Error, BitlyError
		var req  = request("countries", get_url_parms({url}))
		var data = req.get_array_member("countries")
		
		var rv = new dict of string, int64?
		var i  = data.get_length()
		while (i--) > 0
			var uinfo   = data.get_object_element(i)
			var clicks  = uinfo.get_int_member("clicks")
			country: weak Json.Node = uinfo.dup_member("country")
			if country.is_null()
				rv["Unknow"] = clicks
			else
				rv[country.get_string()] = clicks
		
		return rv
	
	
	/**************************************************************************
	 * Returns a list of BitlyInfo containing info for the given short urls.  *
	 *                                                                        *
	 * Bit.ly accepts at most 15 short urls at a given time.                  *
	 *                                                                        *
	 * @param urls: array of url or hash to lookup.                           *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return a list of BitlyInfo.                                           *
	 **************************************************************************/
	def info(urls: array of string): array of BitlyInfo \
	raises Error, BitlyError
		var req  = request("info", get_url_parms(urls))
		var data = req.get_array_member("info")
		
		var rv = new array of BitlyInfo[urls.length]
		var i  = data.get_length()
		while (i--) > 0
			var uinfo       = data.get_object_element(i)
			if uinfo.has_member("error")
				var error = uinfo.get_string_member("error")
				raise new BitlyError.URL_NOT_FOUND(error)
			
			var created_by  = uinfo.get_string_member("created_by")
			var global_hash = uinfo.get_string_member("global_hash")
			var user_hash   = uinfo.get_string_member("user_hash")
			title: weak Json.Node = uinfo.dup_member("title")

			var info = new BitlyInfo(created_by, global_hash, user_hash, null)
			if title is not null
				info.title = title.get_string()
			
			rv[i] = info
		
		return rv
	
	
	/**************************************************************************
	 * Returns a list of the page titles for the given short urls.            *
	 *                                                                        *
	 * Where Bit.ly API returns ``null" for a given title, this API uses an   *
	 * empty string.                                                          *
	 *                                                                        *
	 * @param urls: array of short urls or hashs.                             *
	 * @throw Error                                                           *
	 * @throw BitlyError                                                      *
	 * @return list of strings with the page title.                           *
	 **************************************************************************/
	def titles(urls: array of string): array of string \
	raises Error, BitlyError
		var data = info(urls)
		var rv   = new array of string[data.length]
		
		for var i = 0 to (data.length - 1)
			if data[i].title is null
				rv[i] = ""
			else
				rv[i] = data[i].title
		
		return rv

