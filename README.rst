Gen.ly | Bit.ly API wrapper for Vala/Genie
==========================================

``Gen.ly`` is a wrapper on Bit.ly API for Vala/Genie languages, as well as a
front-end tool for shortening URLs angd getting stats.


Dependencies
============

The library and the front-end tool have a shitload of dependencies. First,
you'll need a Vala and C compiler. Then you can get the packages in the
following list:

- glib-2.0
- gobject-2.0
- gee-2.0
- json-glib-1.0
- gio-2.0
- gtk+-2.0

Gtk+ isn't needed to build the API, but it is needed to build the front end
tools. Vala should come with most of it anyways.

On Ubuntu, you can just type the following at the command line::

    $ sudo apt-get install gcc valac libgee-dev libjson-glib-dev


Compilation and installation
============================

Just ``cd`` to the root tree and type the following::

    $ ./waf configure
    $ ./waf

This should build and install the API and front-ends system wide.
