/* test-suite.gs
 *
 * Suite of tests for Gen.ly API
 */
const LOGIN:  string = "geniely"
const APIKEY: string = "R_4a87acf5573804997b26cdcc36603c77"

//------------------------------------------------------------------------------
namespace BasicTests
	const URL_LIST: array of string = {"http://www.google.com/"
	                                  ,"http://www.deviantart.com/"
	                                  ,"http://www.twitter.com/"}
	api:   Bitly
	surls: list of string
	
	//--------------------------------------------------------------------------
	def shorten()
		try
			for var url in URL_LIST
				surls.add(api.shorten_url(url))
			
			var l = new array of string[URL_LIST.length]
			for var i = 0 to (l.length - 1)
				l[i] = URL_LIST[i]
			api.shorten(l)
			assert true
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def expand()
		try
			for var i = 0 to (URL_LIST.length - 1)
				assert api.expand_url(surls[i]) == URL_LIST[i]
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def bad_expand()
		try
			api.expand_url("http://bad-bad-url")
		except e:BitlyError
			assert true
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def validate()
		try
			assert api.validate(LOGIN, APIKEY)
			assert not api.validate("bad login", "bad apikey")
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def clicks()
		try
			api.clicks({surls[0], surls[1], surls[2]})
			assert true
		except e:Error
			print "Error: %s", e.message
		
		try
			api.clicks({"bad bad url"})
		except e:BitlyError
			assert true
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def referrers()
		try
			api.referrers(surls[2])
			assert true
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def countries()
		try
			api.countries(surls[2])
			assert true
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def titles()
		try
			var data = api.titles({surls[0], surls[1], surls[2]})
			assert data[0] == "Google"
		except e:Error
			print "Error: %s", e.message
	
	
	//--------------------------------------------------------------------------
	def add_all()
		surls = new list of string
		api   = new Bitly(LOGIN, APIKEY)
		
		Test.add_func("/gen.ly/api/shorten", shorten)
		Test.add_func("/gen.ly/api/expand", expand)
		Test.add_func("/gen.ly/api/bad-expand", bad_expand)
		Test.add_func("/gen.ly/api/validate", validate)
		Test.add_func("/gen.ly/api/clicks", clicks)
		Test.add_func("/gen.ly/api/referrers", referrers)
		Test.add_func("/gen.ly/api/countries", countries)
		Test.add_func("/gen.ly/api/titles", titles)


//--[ MAIN ]--------------------------------------------------------------------
init
	Test.init(ref args)
	BasicTests.add_all()
	Test.run()
